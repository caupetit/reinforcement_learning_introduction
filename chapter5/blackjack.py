#!/usr/bin/env python3

#######################################################################
# 2020 Clement AUPETIT (clemaupetit@gmail.com)                        #
# This code was made for learning purposes                            #
# It contains programming exercises and code to produce figures in    #
# the book Reinforcement learning: an introduction 2d edition (2018)  #
# from R.S Sutton and A.G Barto                                       #
# http://incompleteideas.net/book/the-book-2nd.html                   #
#######################################################################

import os
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import tqdm

from functools import reduce

# 52 cards by packet
cards = np.asarray([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10] * 4)
HITS = 1
STICKS = 2
sticks_at_19_20 = np.full((2, 10, 10), HITS)
sticks_at_19_20[:, :, 8:10] = np.full((2, 10, 2), STICKS)

def default_policy_function(P=sticks_at_19_20):
    """
    :param P: an np array of shape(2, 10, 10) filled wit HITS or STICKS integers
    :return: a function that returns and action for a given state depending on the given policy
    """
    def p(S):
        return P[S[0], S[1], S[2]]
    return p

def card_value(count, card):
    if card == 1:
        return 1 if count + 11 > 21 else 11
    return card

# -1 on loose, 0 on draw 1 on win
def game_reward(player, dealer):
    if player > 21:
        return -1
    if dealer > 21:
        return 1
    if player == dealer == 21:
        return 0
    return None

# helper to reduce the first card's
def add(cards_sum, card):
    return cards_sum + card_value(cards_sum, card)


class Player:
    def __init__(self):
        self.sum = 0
        self.usable_ace = False


# S = (usable ace, dealer showing card - 1, player_sum - 12) (indices are reduced to 0 - 10)
# a = preselected_action
def compute_steps_from_episode(S, player: Player, dealer: Player, distributed_cards, policy_function, a=None):
    steps = []
    reward = None
    action = HITS

    def hit_action(player: Player):
        nonlocal distributed_cards
        to_add = card_value(player.sum, cards[distributed_cards])
        distributed_cards += 1
        player.sum += to_add
        player.usable_ace = player.usable_ace or (to_add == 11)
        if player.sum > 21 and player.usable_ace: # use a usable ace
            player.sum -= 10
            player.usable_ace = False
        return player

    while reward is None and action == HITS:
        if a is not None:
            action = a
            a = None
        else:
            action = policy_function(S)

        steps.append((S, action))
        if action == HITS:
            player = hit_action(player)
            S = (int(player.usable_ace), S[1], player.sum - 12)
        reward = game_reward(player.sum, dealer.sum)

    while reward is None:
        action = HITS if dealer.sum < 17 else STICKS
        if action == HITS:
            dealer = hit_action(dealer)
            reward = game_reward(player.sum, dealer.sum)
        else:
            reward = np.sign(player.sum - dealer.sum)
    return steps, reward

def generate_random_first_state():
    np.random.shuffle(cards)

    # every sum under 12 would result in a HIT so these states are skipped
    distributed_cards = 0
    player = Player()
    while player.sum < 12:
        to_add = card_value(player.sum, cards[distributed_cards])
        # where there a usable ace
        player.usable_ace = player.usable_ace or (to_add == 11)
        player.sum += to_add
        distributed_cards += 1

    # distribute dealer's cards
    dealer = Player()
    dealer.sum = reduce(add, cards[distributed_cards:distributed_cards + 2])
    dealer.usable_ace = (cards[distributed_cards:distributed_cards + 2] == 1).any()

    # compute the state for the episode
    S = (
        int(player.usable_ace),                # usable ace in the player's set
        cards[distributed_cards] - 1,   # first card of the dealer reduced to 0 - 10 index
        player.sum - 12,                # sum of player's cards reduced to 0 - 10 index
    )
    return S, player, dealer, distributed_cards + 2

def plot_example_5_2_iteration(subplots, V, N):
    for plot, i in zip(subplots, [1, 0]):
        plot.set_ylabel('Dealer showing')
        plot.set_xlabel('Player sum')
        img = plot.matshow(V[i] / N[i], cmap='Spectral_r')
        plt.colorbar(img, ax=plot)
        plot.xaxis.set_ticks_position('bottom')
        plot.yaxis.set_ticks_position('left')

def figure_5_1():
    fig = plt.figure(figsize=(29.7 / 2.54, 21 / 2.54))
    subplots = fig.subplots(2, 2, sharex=True, sharey=True)
    subplots[0, 0].set_title('After 10 000 episodes')
    subplots[0, 1].set_title('After 500 000 episodes')
    subplots[0, 0].text(-6.0, 4.0, "Usable ace")
    subplots[1, 0].text(-6.0, 4.0, "No usable ace")

    # Values estimates: shape 2 * 100 possible values
    V = np.zeros((2, 10, 10))
    # keep track of the number of times a state is visited to compute the average
    N = np.full(V.shape, 1e-10) # avoid / 0 error

    policy_function = default_policy_function(sticks_at_19_20)
    for k in tqdm.tqdm(range(500000)):
        S, player, dealer, distributed_cards = generate_random_first_state()
        steps, reward = compute_steps_from_episode(S, player, dealer, distributed_cards, policy_function)
        for (s0, s1, s2), _ in steps:
            N[s0][s1][s2] += 1
            V[s0][s1][s2] += reward

        if k == 10000:
            plot_example_5_2_iteration(subplots[:, 0], V, N)
    plot_example_5_2_iteration(subplots[:, 1], V, N)

    plt.setp(subplots,
             xticks=[i for i in range(10)], xticklabels=[str(i) for i in range(12, 22)],
             yticks=[i for i in range(10)], yticklabels=['Ace' if i == 1 else str(i) for i in range(1, 11)])
    fig_path = os.path.dirname(os.path.realpath(__file__)) + f"/figure_5_1.png"
    print(f"Plotting to {fig_path}")
    plt.savefig(fig_path)

def plot_q_values(Qn, name='test'):
    fig = plt.figure(figsize=(21 / 2.54, 29.7 / 2.54))
    subplots = fig.subplots(3, 2, sharex=True, sharey=True)

    def plot(plot, data, title):
        plot.set_title(title)
        img = plot.matshow(data, cmap='Spectral_r')
        plt.colorbar(img, ax=plot)
        plot.xaxis.set_ticks_position('bottom')
        plot.yaxis.set_ticks_position('left')

    plot(subplots[0, 0], Qn[0, 0, :, :], 'Q mean - no usable ace - HIT')
    plot(subplots[0, 1], Qn[0, 1, :, :], 'Q mean - no usable ace - STICK')
    plot(subplots[1, 0], Qn[1, 0, :, :], 'Q mean - usable ace - HIT')
    plot(subplots[1, 1], Qn[1, 1, :, :], 'Q mean - usable ace - STICK')
    P = np.argmax(Qn, 1) # compute greedy policy
    plot(subplots[2, 0], P[0, :, :], 'Policy no usable ace')
    plot(subplots[2, 1], P[1, :, :], 'Policy usable ace')
    plt.setp(subplots,
             xticks=[i for i in range(10)], xticklabels=[str(i) for i in range(12, 22)],
             yticks=[i for i in range(10)], yticklabels=['Ace' if i == 1 else str(i) for i in range(1, 11)])
    fig_path = os.path.dirname(os.path.realpath(__file__)) + f"/{name}.png"
    print(f"Plotting to {fig_path}")
    plt.savefig(fig_path)

def figure_5_2(iterations=500000):
    # Starting policy
    P = sticks_at_19_20.copy()
    # Hit or stick for each possible state and actions
    Q = np.zeros((2, 2, 10, 10))
    # Number of times a couple (S, a) is encountered  (to compute the average)
    N = np.full(Q.shape, 1e-10)  # avoid / 0 error
    # Generate random first actions
    first_actions = np.random.choice([HITS, STICKS], iterations)

    for k in tqdm.tqdm(range(iterations)):
        S, player, dealer, distributed_cards = generate_random_first_state()
        a = first_actions[k]
        policy_function = default_policy_function(P)
        steps, reward = compute_steps_from_episode(S, player, dealer, distributed_cards, policy_function, a)
        for (s0, s1, s2), action in steps:
            N[s0, action - 1, s1, s2] += 1
            Q[s0, action - 1, s1, s2] += reward

            mean = Q[s0, :, s1, s2] / N[s0, :, s1, s2]
            P[s0, s1, s2] = np.argmax(mean) + 1

    plot_q_values(Q / N, 'figure_5_2')

def bonus_on_policy(iterations, epsilon=0.1):
    # Hit or stick for each possible state and actions
    Q = np.zeros((2, 2, 10, 10))
    # Number of times a couple (S, a) is encountered  (to compute the average)
    N = np.full(Q.shape, 1e-10)  # avoid / 0 error

    # setup e-greedy policy
    choose_greedy_action = np.random.choice([True, False], 10000, p=[1 - epsilon + epsilon / 2, epsilon / 2])
    i = 0
    def policy_function(S):
        nonlocal i
        means = (Q / N)[S[0], :, S[1], S[2]] # Q values for each action of the state
        action = np.argmax(means) + 1
        if not choose_greedy_action[i % 10000]:
            # explore the non greedy
            action = HITS if action == STICKS else STICKS
        i += 1
        return action

    for k in tqdm.tqdm(range(iterations)):
        S, player, dealer, distributed_cards = generate_random_first_state()
        steps, reward = compute_steps_from_episode(S, player, dealer, distributed_cards, policy_function)
        for (s0, s1, s2), action in steps:
            N[s0, action - 1, s1, s2] += 1
            Q[s0, action - 1, s1, s2] += reward

    plot_q_values(Q / N, 'on_policy')

def figure_5_3():
    player = Player()
    dealer = Player()

    def random_policy(S):
        return np.random.choice([HITS, STICKS])

    # 100 runs of 10000 episodes
    num_iterations = 100
    vals = np.zeros((10000, 2))
    for _ in tqdm.tqdm(range(num_iterations)):
        v = 0
        t = 0
        r = 0
        for n in range(10000):
            S = (1, 2 - 1, 13 - 12) # usable ace, dealer showing 2 player score: 13
            player.usable_ace = True
            player.sum = 13
            dealer.sum = int(np.random.random() * 10) + 2
            dealer.usable_ace = np.random.random([0, 1])
            np.random.shuffle(cards)

            steps, reward = compute_steps_from_episode(S, player, dealer, 5, policy_function=random_policy)

            ratio = 1
            for ((s0, s1, s2), action) in steps:
                # p(a|s) = 0 in those cases => break as * 0
                if (action == STICKS and s2 < 20 - 12) or (action == HITS and s2 > 19 - 12):
                    ratio = 0
                    break
                ratio *= 2 # 2 = 1.0 / 0.5 = p(a|s) / b(a|s)
            r += ratio
            v += reward * ratio
            t += 1
            vals[n][0] += v / t
            vals[n][1] += v / r if r else 0

    print(vals / num_iterations)
    mean_square_errors = (vals / num_iterations - -0.27726) ** 2
    plt.plot(mean_square_errors[:,0], label='Ordinary importance sampling')
    plt.plot(mean_square_errors[:,1], label="Weighted importance sampling")
    plt.legend()
    plt.xscale('log')
    fig_path = os.path.dirname(os.path.realpath(__file__)) + f"/figure_5_3.png"
    print(f"Plotting to {fig_path}")
    plt.savefig(fig_path)

# 20 or 21: 1 / 0.5 => 2
# 0 -> 20:  0 / 0.5

if __name__ == "__main__":
    # figure_5_1()
    # figure_5_2(1000000 * 5)
    # bonus_on_policy(500000 * 10)
    figure_5_3()

