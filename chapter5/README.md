# Monte Carlo methods

The focus here is to solve MDP's via stochastic exploration of the state and actions space.

## Policy evaluation

For a given policy, evaluate each step over an episode:
 
Sum the obtained rewards and number of time a state is visited for the first time during an episode to have a mean value
- always follow policy
- generate a random first state for exploration

See figure_5_1

## Monte Carlo control

Update the policy greedily: evaluate and choose the action that has the best evaluation,
this on each step of each episode => tends to v* and p*

- On policy => use the same policy to evaluate and update

### MonteCarlo ES
On policy method that uses random starts to explore the state and actions space

See figure_5_2

### On-policy first-visit

On policy with e-greedy policy: no random start, pick a random action with e/A probability to explore
This is kind of more realistic for 'real world' MDP's where random start can't be taken

See bonus_on_policy

### Off-policy Prediction via Importance Sampling

- Off policy => use a target policy p (the learned one) and a behavior policy b (and exploring one)
    
    b must be able to take the same actions as p with at least a little probability
    p can and may be deterministic (aka greedy all the time)

#### Importance sampling

Evaluates p using b with the importance-sampling ratio

- importance sampling ratio

multiply all the probabilities that a selected action had for each time step following p
then divide it bye the same thing following b

- ordinary importance sampling

=> multiply the previously obtained ratio with the rewards of each state and get the mean, this gives V(s) for p using b

- weighted importance sampling

