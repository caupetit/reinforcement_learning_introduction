#!/usr/bin/env python3

#######################################################################
# 2019 Clement AUPETIT (clemaupetit@gmail.com)                        #
# Permission given to modify the code as long as you keep this        #
# declaration at the top                                              #
#######################################################################

import random
import signal
import sys

EMPTY = -1

# When selecting greedy moves, an unexplored state will have DEFAULT_PROB as estimated value
# Setting the DEFAULT_DRAW_PROB to a value lower than DEFAULT_PROD, will have as effect to encourage
# the greedy selection to choose unexplored moves => Learns faster
WIN_REWARD = 1              # 100% chances to win
LOOSE_REWARD = 0            # 0% chances to win
DEFAULT_DRAW_PROB = 0.25    # Specifying that draw is better than loose
DEFAULT_PROB = 0.5          # 50% chance to win (unvisited states)


class RandomPlayer:
    _default_results = {'win': 0, 'loose': 0, 'draw': 0, 'X': 0, 'O': 0}

    def __init__(self):
        self.id = -1
        self.results = dict(self._default_results)

    def reset(self, id):
        assert(id in [0, 1])
        self.id = id

    def reset_results(self, options=None):
        self.results = dict(self._default_results)

    """
    state: is a list of 9 integers with EMPTY (-1), 0 or 1 as values
    returns: an integer between 0 and 8 where state[i] == EMPTY
    """
    def action(self, state):
        valid_actions = [i for (i, s) in enumerate(state) if s == EMPTY]
        return random.choice(valid_actions)

    def feedback(self, winner_id):
        assert(self.id != -1)
        assert(winner_id in [EMPTY, 0, 1])
        result_map = {
            EMPTY: 'draw',
            0: 'loose',
            1: 'win'
        }
        symbol_map = {0: 'X', 1: 'O'}
        result = winner_id if winner_id == -1 else int(winner_id == self.id)
        self.results[result_map[result]] += 1
        self.results[symbol_map[self.id]] += 1


class HumanPlayer(RandomPlayer):
    def action(self, state):
        valid_actions = [i for (i, s) in enumerate(state) if s == EMPTY]
        keys = ['q', 'w', 'e', 'a', 's', 'd', 'z', 'x', 'c']
        valid_keys = [keys[a] for a in valid_actions]
        while 1:
            action = input(f"Player #{self.id + 1} plays: {valid_keys}: ")
            try:
                action = keys.index(action)
                valid_actions.index(action)
                return action
            except ValueError:
                print(f"Wrong action selected:, possible actions are mapped as:\n"
                      "q | w | e\n"
                      "a | s | d\n"
                      "z | x | c\n")


class AiPlayer(RandomPlayer):
    def __init__(self, epsilon=0.1, learning_rate=0.1):
        super(AiPlayer, self).__init__()
        self.states_hashes = []
        self.epsilon = epsilon
        self._epsilon = epsilon
        self.learning_rate = learning_rate
        self.exploratory_moves = []
        self.learn_on_exploration_moves = True
        self.estimates = dict()

    def reset(self, id):
        super(AiPlayer, self).reset(id)
        self.states_hashes = []
        self.exploratory_moves = []

    def reset_results(self, options=None):
        options = options if options else {}
        super(AiPlayer, self).reset_results(options)
        mode = options.get('mode', None)
        if mode == 'train':
            self.epsilon = self._epsilon
        else:
            self._epsilon = self.epsilon
            self.epsilon = 0.0

    def hash_for_action(self, state, action):
        state[action] = self.id
        state_hash = tuple(state)
        state[action] = EMPTY
        return state_hash

    def action(self, state):
        valid_actions = [i for (i, s) in enumerate(state) if s == EMPTY]

        # Exploration
        if random.random() < self.epsilon:
            action = random.choice(valid_actions)
            state_hash = self.hash_for_action(state, action)
            self.states_hashes.append(state_hash)
            # http://fumblog.um.ac.ir/gallery/839/weatherwax_sutton_solutions_manual.pdf exercise 1.4
            if not self.learn_on_exploration_moves:
                self.exploratory_moves.append(len(self.states_hashes) - 1)
            return action

        # Greedy choices
        random.shuffle(valid_actions)
        choices = []
        for action in valid_actions:
            state_hash = self.hash_for_action(state, action)
            estimate = self.estimates.get(state_hash, DEFAULT_PROB)
            choices.append( (state_hash, action, estimate) )
        choice = max(choices, key=lambda x: x[2])
        self.states_hashes.append(choice[0])
        return choice[1]

    def feedback(self, winner_id):
        super(AiPlayer, self).feedback(winner_id)
        end_state_estimate = DEFAULT_DRAW_PROB if winner_id == EMPTY else int(winner_id == self.id)
        self.estimates[self.states_hashes[-1]] = end_state_estimate
        for i in reversed(range(len(self.states_hashes) - 1)):
            if i in self.exploratory_moves:
                continue
            estim = self.estimates.get(self.states_hashes[i], DEFAULT_PROB)
            estim_next = self.estimates.get(self.states_hashes[i + 1], DEFAULT_PROB)
            self.estimates[self.states_hashes[i]] = estim + self.learning_rate * (estim_next - estim)


def print_state(state):
    symbol_map = {-1: ' ', 0: 'X', 1: 'O'}
    str_state = [symbol_map[s] for s in state]
    rows = [" | ".join(str_state[i:i+3]) for i in [0, 3, 6]]
    print("----------\n" + "\n".join(rows) + '\n----------')


# grid ids
# 0 1 2
# 3 4 5
# 6 7 8
# These are the checks to do for a win
# one line of checks for each possible action on the state
checks = [
    [[0, 1, 2], [0, 3, 6], [0, 4, 8]],
    [[0, 1, 2], [1, 4, 7]],
    [[0, 1, 2], [2, 4, 6], [2, 5, 8]],
    [[0, 3, 6], [3, 4, 5]],
    [[3, 4, 5], [0, 4, 8], [1, 4, 7], [2, 4, 6]],
    [[3, 4, 5], [2, 5, 8]],
    [[0, 3, 6], [6, 4, 2], [6, 7, 8]],
    [[6, 7, 8], [1, 4, 7]],
    [[6, 7, 8], [0, 4, 8], [2, 5, 8]]
]


def play(player1, player2, verbose=False):
    turns = 0
    players = [player1, player2]
    state = [EMPTY] * 9

    if verbose:
        print_state(state)

    while 1:
        # Get current player
        player_id = int(turns % 2 != 0)
        player = players[player_id]

        # get player move
        action = player.action(state)
        assert state[action] == EMPTY
        state[action] = player_id

        if verbose:
            print_state(state)
        turns += 1

        # End game in case of win or draw
        def line_filled(line_to_check):
            ids = [state[c] for c in line_to_check]
            return all([player_id == i for i in ids])
        won = any([line_filled(line) for line in checks[action]])
        if won:
            return player_id
        draw = all([s != EMPTY for s in state])
        if draw:
            return EMPTY


def run_batch(player1, player2, num_games, verbose=False):
    batch_stats = [0, 0, 0]
    players = [player1, player2]
    for n in range(num_games):
        random.shuffle(players)
        for i, player in enumerate(players):
            player.reset(i)
        winner = play(*players, verbose)
        if verbose:
            if winner == EMPTY:
                print("This is a draw")
            else:
                print(f"Player {winner + 1} won")
        players[0].feedback(winner)
        players[1].feedback(winner)
        batch_stats[winner + 1] += 1
        if n != 0 and n % 1000 == 0:
            print(n, [f"{what}: {s / n:0.3f}" for (s, what) in zip(batch_stats, ['draw', 'win X', 'win O'])])
    print(f"Batch run: [draw, win X, win O]: {batch_stats}")


def train(p1, p2, iterations):
    p1.reset_results({'mode': 'train'})
    p2.reset_results({'mode': 'train'})
    run_batch(p1, p2, iterations)
    print(p1.__class__.__name__, p1.results)
    print(p2.__class__.__name__, p2.results)


def evaluate(p1, p2):
    p1.reset_results()
    p2.reset_results()
    run_batch(p1, p2, int(1e3))
    print(p1.__class__.__name__, p1.results)
    print(p2.__class__.__name__, p2.results)


def test(player):
    player.reset_results()

    def sigint_handler(s, f):
        print('\nTested player: ', player.results)
        sys.exit(0)
    signal.signal(signal.SIGINT, sigint_handler)
    run_batch(player, HumanPlayer(), int(1e10), verbose=True)
    signal.signal(signal.SIGINT, signal.default_int_handler)


if __name__ == '__main__':
    ai1 = AiPlayer()
    ai2 = AiPlayer(learning_rate=0.5)

    train(ai1, ai2, int(3e4))
    evaluate(ai1, RandomPlayer())
    evaluate(ai1, ai2)
    test(ai1)
