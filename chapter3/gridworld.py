#!/usr/bin/env python3

#######################################################################
# Copyright (C)                                                       #
# 2019 Clement AUPETIT (clemaupetit@gmail.com)                        #
# 2016-2018 Shangtong Zhang(zhangshangtong.cpp@gmail.com)             #
# 2016 Tian Jun(tianjun.cpp@gmail.com)                                #
# 2016 Artem Oboturov(oboturov@gmail.com)                             #
# 2016 Kenta Shimada(hyperkentakun@gmail.com)                         #
# Permission given to modify the code as long as you keep this        #
# declaration at the top                                              #
#######################################################################

# Too much of the official implementation in this one too

import os

import matplotlib
matplotlib.use('Agg')

import numpy as np
import matplotlib.pyplot as plt

GAMMA = 0.9

A = np.asarray((1, 0))
ANext = np.asarray((1, 4))

B = np.asarray((3, 0))
BNext = np.asarray((3, 2))

# North, South, East, West
actions = np.asarray([
    (0, -1), (0, 1), (-1, 0), (1, 0)
])


def register_figure(data, file_name):
    plt.title(file_name)
    fig, ax = plt.subplots()
    ax.set_axis_off()
    table = plt.table(cellText=data, loc='center', cellLoc='center')
    table.scale(0.5, 3)

    fig_path = os.path.dirname(os.path.realpath(__file__)) + f"/{file_name}"
    print(f"Plotting to {fig_path}")
    plt.savefig(fig_path)
    plt.close()


def next_state(s, a):
    if (s == A).all():
        return ANext, 10
    if (s == B).all():
        return BNext, 5
    next = s + a
    if any(next > 4) or any(next < 0):
        return s, -1
    return next, 0


def vp_list_by_action(i, j, all_vp):
    next = [next_state([i, j], a) for a in actions]
    return [r + GAMMA * all_vp[x, y] for ((x, y), r) in next]


def random_policy_state_value(i, j, all_vp):
    return sum(vp_list_by_action(i, j, all_vp)) * 0.25


def optimal_state_value(i, j, all_vp):
    return max(vp_list_by_action(i, j, all_vp))


def compute(state_value_function):
    delta = 1
    vps = np.zeros((5, 5))
    while delta > 1e-2:
        new_vp = np.zeros_like(vps)
        for i in range(5):
            for j in range(5):
                new_vp[i][j] = state_value_function(i, j, vps)
        delta = np.abs(new_vp - vps).sum()
        vps = new_vp
    return vps


if __name__ == '__main__':
    print('figure 3.2')
    data = compute(random_policy_state_value).transpose().round(1)
    print(data)
    register_figure(data, 'figure_3_2.png')
    print('figure 3.5')
    data = compute(optimal_state_value).transpose().round(1)
    print(data)
    register_figure(data, 'figure_3_5.png')

