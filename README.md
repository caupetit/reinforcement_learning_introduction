# My own python implem of Reinforcement Learning: An Introduction

[Reinforcement Learning: An Introduction](http://incompleteideas.net/book/the-book-2nd.html) 
from Richard S. Sutton
and Andrew G. Barto

I try to make my own implementation of the code for learning purposes

Inspired from the existing [python implementation](https://github.com/ShangtongZhang/reinforcement-learning-an-introduction)

## setup
I use virtualenv
```
pip3 install virtualenv
python3 -m virtualenv venv --python=python3
source ./venv/bin/activate
```

## Requirements

### made with Python 3.6.8
`pip install -r requirements.txt`
* as of chapter2
    * matplotlib, numpy and tqdm
* as of chapter 4
    * scipy for distributions

### Chapter 1
play vs tic_tac_toe ai
```
python3 chapter1/tic_tac_toe.py
```

### Chapter 2
generate figures to compare algorithms
```
python3 chapter2/k_bandit.py
```
Or to launch silently in the background for long computations (on a remote machine for example)
```bash
nohup bash -c "time ./chapter2/k_bandit.py" > run_chapter2.out &
```
