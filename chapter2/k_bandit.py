#!/usr/bin/env python3

#######################################################################
# Copyright (C)                                                       #
# 2019 Clement AUPETIT (clemaupetit@gmail.com)                        #
# 2016-2018 Shangtong Zhang(zhangshangtong.cpp@gmail.com)             #
# 2016 Tian Jun(tianjun.cpp@gmail.com)                                #
# 2016 Artem Oboturov(oboturov@gmail.com)                             #
# 2016 Kenta Shimada(hyperkentakun@gmail.com)                         #
# Permission given to modify the code as long as you keep this        #
# declaration at the top                                              #
#######################################################################

# Too much of the official implementation in this one

from collections import namedtuple
from fractions import Fraction
import os
import sys
import multiprocessing as mp

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.style as mplstyle
import numpy as np
import tqdm

mplstyle.use(['seaborn-darkgrid'])

this_dir_path = os.path.dirname(os.path.realpath(__file__))

SAMPLE_AVERAGE = 'sp'
INCREMENTAL_SAMPLE_AVERAGE = 'isp'
WEIGHTED_AVERAGE = 'wa'
GRADIENT_ASCENT = 'ga'


class KBanditGenerator:
    def __init__(self, k, n, std=1.0, qt=0.0, memory=None):
        self.qt = np.random.randn(k) + qt  # setup the default means for the rewards
        self.raws = np.random.normal(loc=self.qt,   # pre-generate all possible results for qt and std
                                     scale=std,
                                     size=(n, k))
        memory = n if memory is None else memory
        self._actions = np.zeros(memory)
        self._rewards = np.zeros(memory)
        self.memory = memory
        self.n = n

    def best_actions(self):
        return self._actions == self.qt.argmax()

    def rewards(self):
        return self._rewards

    def next_reward(self, i, action):
        reward = self.raws[i, action]
        self._actions[(i - self.memory) % self.memory] = action
        self._rewards[(i - self.memory) % self.memory] = reward
        return reward


class KBanditNonStationaryGenerator:
    def __init__(self, k, n, std=1.0, qt=0.0, memory=None):
        self.std = std
        self.qt = np.random.randn(k) + qt  # setup the default means for the rewards
        self.deviations = np.random.normal(loc=0.0, scale=0.01, size=(n, k))  # pre-generate the means evolution
        memory = n if memory is None else memory
        self._best_actions = np.zeros(memory)
        self._rewards = np.zeros(memory)
        self.memory = memory

    def best_actions(self):
        return self._best_actions

    def rewards(self):
        return self._rewards

    def next_reward(self, i, action):
        reward = self.std * np.random.randn() + self.qt[action]
        self._rewards[(i - self.memory) % self.memory] = reward
        self._best_actions[(i - self.memory) % self.memory] = action == self.qt.argmax()
        self.qt += self.deviations[i]
        return reward


class LearnConfig:
    # Utility class to have some kind of parameter validation, documentation
    # and to use pool.imap() cause I want a fancy progress bar even if slower than map
    def __init__(self, epsilon=0.0, k=10, n=1000, std=1.0, qt=0.0, q1=0.0, ucb=0.0,
                 estimation=SAMPLE_AVERAGE, alpha=0.1, baseline=True,
                 generator_class=KBanditGenerator, memory=None):
        self.epsilon = float(epsilon)   # the rate of the non greedy actions selection
        self.k = int(k)                 # number of arms of the k-armed bandit problem
        self.n = int(n)                 # number of steps for a single k-armed bandit problem
        self.std = float(std)           # the standard deviation for the number generation of the ka bandit problem
        self.qt = qt if qt is None else float(qt)   # the base mean of the generated numbers for all arms
        self.q1 = float(q1)             # the first estimation value for all arms
        self.ucb = float(ucb)           # ratio for uncertainty of greedy choices (book part 2.7)
        self.estimation_method = estimation         # enum of the estimation function to use
        self.alpha = float(alpha)                   # step size parameter for WEIGHTED_AVERAGE and GRADIENT_ASCENT
        self.baseline = bool(baseline)              # enable mean reward baseline in gradient estimation
        self.generator_class = generator_class      # the random number generator for the ka bandit problem
        self.memory = memory if not memory else int(memory) # max number of turns to keep in memory
        assert estimation in [SAMPLE_AVERAGE, INCREMENTAL_SAMPLE_AVERAGE, WEIGHTED_AVERAGE, GRADIENT_ASCENT]
        assert generator_class in [KBanditGenerator, KBanditNonStationaryGenerator]

    def tuple(self):
        return self.epsilon, self.k, self.n, self.std, self.qt, self.q1, self.ucb,\
               self.estimation_method, self.alpha, self.generator_class, self.memory

    def __str__(self):
        return str(vars(self))


def learn(learn_config):
    (epsilon, k, n, std, qt, q1, ucb, estimation, alpha, generator_class, m) = learn_config.tuple()
    np.random.seed(int.from_bytes(os.urandom(4), byteorder=sys.byteorder))   # for multiprocessing
    generator = generator_class(k, n, std, qt, m)
    random_actions = np.random.random_integers(0, k - 1, size=n)        # pre-generate random actions
    explore = np.random.rand(n) < epsilon                               # pre-generate e-greed choices

    sum_rewards = np.zeros(k)
    actions_count = np.full(k, 1e-10)  # avoid / 0
    qta = np.full(k, q1)       # reward estimations

    def sample_average_estimation():
        qta[action] = sum_rewards[action] / actions_count[action]

    def incremental_sample_average_estimation():
        qta[action] += 1/actions_count[action] * (reward - qta[action])

    def weighted_average_estimation():
        qta[action] += alpha * (reward - qta[action])

    probabilities = None
    average_reward = 0.0

    def gradient_ascent_estimation():
        nonlocal qta
        nonlocal average_reward
        one_hot = np.zeros(k)
        one_hot[action] = 1
        qta += alpha * (reward - average_reward) * (one_hot - probabilities)
        if learn_config.baseline:
            average_reward += 1 / (i + 1) * (reward - average_reward)

    def gradient_ascent_action():
        nonlocal qta
        nonlocal probabilities
        e_qta = np.exp(qta)
        probabilities = e_qta / np.sum(e_qta)
        return np.random.choice(range(k), p=probabilities)

    def greedy_action():
        # no need to pick a random maximum here, equality happens too rarely to be significant
        return qta.argmax()

    def ucb_action():
        # loop from 1 to 1001 for ln
        return (qta + ucb * np.sqrt(np.log(i + 1) / actions_count)).argmax()

    action_selection = greedy_action
    if ucb:
        action_selection = ucb_action
    if estimation == GRADIENT_ASCENT:
        action_selection = gradient_ascent_action

    update_qta = {
        SAMPLE_AVERAGE: sample_average_estimation,
        INCREMENTAL_SAMPLE_AVERAGE: incremental_sample_average_estimation,
        WEIGHTED_AVERAGE: weighted_average_estimation,
        GRADIENT_ASCENT: gradient_ascent_estimation,
    }[estimation]
    for i in range(n):
        if explore[i]:
            action = random_actions[i]
        else:
            action = action_selection()
        reward = generator.next_reward(i, action)

        sum_rewards[action] += reward
        actions_count[action] += 1
        update_qta()

    return generator.best_actions(), generator.rewards()


def ten_armed_test_bed(nb_learns=2000, learn_configs=None, verbose=-1):
    learn_configs = learn_configs if learn_configs else [LearnConfig()]
    mean_best_moves, mean_rewards = [], []

    with mp.Pool(os.cpu_count() // 2) as pool:
        for learn_config in learn_configs:
            if verbose > 0:
                print(f"launching {learn_config}")

            args_generator = (learn_config for _ in range(nb_learns))
            if verbose != 0:
                r = list(tqdm.tqdm(pool.imap(learn, args_generator), total=nb_learns))
            else:  # faster
                r = pool.map(learn, args_generator)
            all_best_moves, all_rewards = zip(*r)
            mean_best_moves.append(np.array(all_best_moves).mean(0))
            mean_rewards.append(np.array(all_rewards).mean(0))

            if verbose > 0:
                print(f'mean_best_moves: {mean_best_moves[-1].mean():0.3f} '
                      f'mean_rewards: {mean_rewards[-1].mean():0.3f}')

    return mean_best_moves, mean_rewards


def plot_random_distribution(k, n, std):
    qt = np.random.randn(k)
    raws = np.random.normal(loc=qt, scale=std, size=(n, k))
    plt.violinplot(dataset=raws)
    plt.xlabel("Action")
    plt.ylabel("Reward distribution")
    fig_path = this_dir_path + f"/figure_2_1_k{k}_n{n}_std{std}.png"
    print(f"Ploting generated random distribution to {fig_path}")
    plt.savefig(fig_path)
    plt.close()


def register_plot(file_name, info="Plotting to: "):
    plt.legend()
    fig_path = this_dir_path + f"/{file_name}"
    print(f"{info} {fig_path}")
    plt.savefig(fig_path)
    plt.close()


def default_plot(mean_best_moves, mean_rewards, epsilons, fig_name='current_test'):
    plt.figure(figsize=(10, 20))
    plt.subplot(2, 1, 1)
    for eps, rewards in zip(epsilons, mean_rewards):
        plt.plot(rewards, label='epsilon = %.02f' % eps)
    plt.xlabel('steps')
    plt.ylabel('average reward')
    plt.legend()

    plt.subplot(2, 1, 2)
    for eps, counts in zip(epsilons, mean_best_moves):
        plt.plot(counts, label='epsilon = %.02f' % eps)
    plt.xlabel('steps')
    plt.ylabel('% optimal action')

    register_plot(fig_name)


def figure_2_1():
    print("figure 2.1")
    plot_random_distribution(k=10, n=1000, std=1)
    plot_random_distribution(k=10, n=1000, std=10)


def figure_2_2(epsilons=None):
    print("figure 2.2")
    epsilons = epsilons if epsilons else [0.0, 0.01, 0.1]
    configs = [LearnConfig(epsilon=e) for e in epsilons]
    bm1, r1 = ten_armed_test_bed(learn_configs=configs)
    default_plot(bm1, r1, epsilons, 'figure_2_2.png')

    configs = [LearnConfig(epsilon=e, std=10) for e in epsilons]
    bm1, r1 = ten_armed_test_bed(learn_configs=configs)
    default_plot(bm1, r1, epsilons, 'figure_2_2_std_10.png')


def figure_2_3():
    print("figure 2.3")
    configs = [LearnConfig(q1=5.0, estimation=WEIGHTED_AVERAGE),
               LearnConfig(q1=0.0, estimation=WEIGHTED_AVERAGE)]
    best_moves, _ = ten_armed_test_bed(learn_configs=configs)
    plt.plot(best_moves[0], label='Optimistic: q=5, e=0.0')
    plt.plot(best_moves[1], label='Realistic: q=0, e=0.1')
    plt.xlabel('steps')
    plt.ylabel('% optimal action')
    register_plot("figure_2_3.png", "Plotting optimistic greedy vs realistic e=greedy: ")


def exercise_2_5(epsilons=None):
    print("Exercise 2.5: ~ 10 minutes")
    epsilons = epsilons if epsilons else [0.0, 0.01, 0.1]
    def generate_conf(e):
        return LearnConfig(epsilon=e, n=10000, estimation=WEIGHTED_AVERAGE,
                           generator_class=KBanditNonStationaryGenerator)
    configs = [generate_conf(e) for e in epsilons]
    bm, r = ten_armed_test_bed(learn_configs=configs)
    default_plot(bm, r, epsilons, fig_name='exercise_2.5_weighted.png')
    for conf in configs:
        conf.estimation_method = INCREMENTAL_SAMPLE_AVERAGE
    bm, r = ten_armed_test_bed(learn_configs=configs)
    default_plot(bm, r, epsilons, fig_name='exercise_2.5_incremental.png')


def figure_2_4():
    print("figure 2.4")
    configs = [LearnConfig(ucb=2.0), LearnConfig(epsilon=0.1)]
    _, r = ten_armed_test_bed(learn_configs=configs)
    plt.plot(r[0], label='ucb c = 2')
    plt.plot(r[1], label='e-greedy e = 0.1')
    plt.xlabel('steps')
    plt.ylabel('Average reward')
    register_plot('figure_2_4.png')


def figure_2_5():
    print("figure 2.5")
    configs = [LearnConfig(estimation=GRADIENT_ASCENT, qt=4.0, alpha=0.1),
               LearnConfig(estimation=GRADIENT_ASCENT, qt=4.0, alpha=0.4),
               LearnConfig(estimation=GRADIENT_ASCENT, qt=4.0, alpha=0.1, baseline=False),
               LearnConfig(estimation=GRADIENT_ASCENT, qt=4.0, alpha=0.4, baseline=False)]
    bms, _ = ten_armed_test_bed(learn_configs=configs)
    plt.plot(bms[0], label='baseline alpha=0.1')
    plt.plot(bms[1], label='baseline alpha=0.4')
    plt.plot(bms[2], label='baseline=0 alpha=0.1')
    plt.plot(bms[3], label='baseline=0 alpha=0.4')
    plt.xlabel('steps')
    plt.ylabel('% optimal action')
    register_plot('figure_2_5.png', 'Plotting gradient ascent comparison')


ParamsStudy = namedtuple('ParamsStudy', ['params', 'conf_builder', 'plot_format', 'label'])


def study_params(studies, figure_name, iter_num=2000):
    for c in studies:
        print(c.label)
        configs = [c.conf_builder(p) for p in c.params]
        _, mean_rewards = ten_armed_test_bed(nb_learns=iter_num, learn_configs=configs)
        mean_rewards = list(map(lambda r: r.mean(), mean_rewards))
        plt.plot(c.params, mean_rewards, c.plot_format, label=c.label)

    plt.xscale('log')
    plt.xlabel('parameter')
    plt.ylabel('Mean rewards')
    X = np.geomspace(1/128, 4, num=10)
    X_labels = [Fraction(item).limit_denominator() for item in X]
    plt.xticks(np.unique(X), X_labels)
    register_plot(figure_name, "Plotting algorithms parameter study to: ")


def figure_2_6():
    print("figure 2.6 - get ready for about 10 minutes")
    studies = [
        ParamsStudy(np.geomspace(1/128, 1/4, num=6),
                    lambda p: LearnConfig(epsilon=p, estimation=INCREMENTAL_SAMPLE_AVERAGE),
                    plot_format='r', label='e-greedy'),
        ParamsStudy(np.geomspace(1/16, 4, num=6),
                    lambda p: LearnConfig(ucb=p, estimation=INCREMENTAL_SAMPLE_AVERAGE),
                    plot_format='b', label='UCB'),
        ParamsStudy(np.geomspace(1/4, 4, num=6),
                    lambda p: LearnConfig(q1=p, estimation=WEIGHTED_AVERAGE),
                    plot_format='k', label='optimistic greedy'),
        ParamsStudy(np.geomspace(1/32, 3, num=6),
                    lambda p: LearnConfig(alpha=p, estimation=GRADIENT_ASCENT),
                    plot_format='g', label='gradient bandit')
    ]
    study_params(studies, 'figure_2_6.png')


def exercise_2_11():
    print("exercise 2.11 - Will take 2 or 3 days. I hope you have enough memory (~ 6Go with 2 processes)")
    learn_iter = 200000
    memory = learn_iter // 2
    studies = [
        ParamsStudy(np.geomspace(1/128, 1/4, num=6),
                    lambda p: LearnConfig(epsilon=p, estimation=INCREMENTAL_SAMPLE_AVERAGE, memory=memory,
                                          generator_class=KBanditNonStationaryGenerator, n=learn_iter),
                    plot_format='r', label='e-greedy'),
        ParamsStudy(np.geomspace(1/16, 4, num=6),
                    lambda p: LearnConfig(ucb=p, estimation=INCREMENTAL_SAMPLE_AVERAGE, memory=memory,
                                          generator_class=KBanditNonStationaryGenerator, n=learn_iter),
                    plot_format='b', label='UCB'),
        ParamsStudy(np.geomspace(1/4, 4, num=6),
                    lambda p: LearnConfig(q1=p, estimation=WEIGHTED_AVERAGE, memory=memory,
                                          generator_class=KBanditNonStationaryGenerator, n=learn_iter),
                    plot_format='k', label='optimistic greedy'),
        ParamsStudy(np.geomspace(1/32, 3, num=6),
                    lambda p: LearnConfig(alpha=p, estimation=GRADIENT_ASCENT, memory=memory,
                                          generator_class=KBanditNonStationaryGenerator, n=learn_iter),
                    plot_format='g', label='gradient bandit'),
        ParamsStudy(np.geomspace(1/128, 1/4, num=6),
                    lambda p: LearnConfig(epsilon=p, alpha=0.1, estimation=WEIGHTED_AVERAGE, memory=memory,
                                          generator_class=KBanditNonStationaryGenerator, n=learn_iter),
                    plot_format='y', label='constant step size e-greedy')
    ]
    study_params(studies, 'exercise_2_11.png')


if __name__ == '__main__':
    print("Launching generation of all figures, some will require some time."
          "(exercise 2.11 is disabled as too huge)")
    figure_2_1()
    figure_2_2()
    figure_2_3()

    exercise_2_5()

    figure_2_4()
    figure_2_5()
    figure_2_6()

    # this one took 56 hours on my computer with too much memory so i won't let it as example
    # exercise_2_11()
