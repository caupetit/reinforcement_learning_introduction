#!/usr/bin/env python3

#######################################################################
# 2020 Clement AUPETIT (clemaupetit@gmail.com)                        #
# This code was made for learning purposes                            #
# It contains programming exercises and code to produce figures in    #
# the book Reinforcement learning: an introduction 2d edition (2018)  #
# from R.S Sutton and A.G Barto                                       #
# http://incompleteideas.net/book/the-book-2nd.html                   #
#######################################################################

import os
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

MAX_CAPITAL = 100

PH = 0.4
GAMMA = 1.0

# all possible states with 0 and 100 included (terminal ones)
S = np.arange(MAX_CAPITAL + 1)

len_states = S.shape[0]

# all possible actions for each states
A = [np.arange(1, min(s, MAX_CAPITAL - s) + 1) for s in S[:-1]]


def next_values_for_action(s, actions, V):
    return [GAMMA * V[s + a] * PH + (1 - PH) * GAMMA * V[s - a] for a in actions]


def gambler_problem(name):
    plt.figure(figsize=(21 / 2.54, 29.7 / 2.54))
    plt.subplot(2, 1, 1, xlabel='Capital', ylabel='Value estimates')
    plt.xticks([1, 25, 50, 75, 99])

    # all possible value estimates. The terminal state 100 has a reward of 1
    V = np.zeros(len_states)
    V[-1] = 1

    delta = 1
    k = 0
    while delta > 1e-8 and k < 1000:
        delta = 0
        for i, s in enumerate(S[1:-1]):
            i = i + 1
            v = V[i]
            V[i] = np.amax(next_values_for_action(s, A[i], V))
            delta = max(delta, abs(v - V[i]))
        k += 1
        if k in [1, 2, 3, 50, 500]:
            plt.plot(V, label='k = {}'.format(k))
    plt.plot(V, label='Best values: k = {}'.format(k))

    Pa = np.zeros(MAX_CAPITAL)
    for i, s in enumerate(S[1:-1]):
        i = i + 1
        # https://github.com/ShangtongZhang/reinforcement-learning-an-introduction/issues/83
        values = np.round(next_values_for_action(s, A[i], V), 5)
        Pa[i] = A[i][np.argmax(values)]

    plt.title(f'PH = {PH}')
    plt.legend(loc='best')
    plt.subplot(2, 1, 2, xlabel='Capital', ylabel='Final policy (stake)')
    plt.xticks([1, 25, 50, 75, 99])
    plt.plot(Pa)
    fig_path = os.path.dirname(os.path.realpath(__file__)) + f"/{name}.png"
    print(f"Plotting to {fig_path}")
    plt.savefig(fig_path)


if __name__ == '__main__':
    gambler_problem('figure_4_3')

    PH = 0.25
    gambler_problem('exercise_4_9_025')

    PH = 0.55
    gambler_problem('exercise_4_9_055')
    # The result of this one confused me at first, but we're trying to find the policy that maximises
    # the chances of win with the lowest possible bet (with the rounding). Not the one that goes the fastest
    # to the win. Even if a ph < 0.5 as for effect to minimize the number of bets, this is not the case for
    # ph >= 0.5

