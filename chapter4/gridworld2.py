#!/usr/bin/env python3

#######################################################################
# 2019 Clement AUPETIT (clemaupetit@gmail.com)                        #
# Permission given to modify the code as long as you keep this        #
# declaration at the top                                              #
#######################################################################

import os

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

size = 4  # Grid border size (size * size grid)

# Possible actions for each position in the grid (i, size * j)
# East, West, North, South
actions = (
    (-1, 0), (1, 0), (0, -1), (0, 1)
)

TERMINAL_STATES = (
    0, size * size - 1
)

# Used in main to add ex4.2 states to the problem
CUSTOM_STATES = {}


def register_figure(file_name):
    fig_path = os.path.dirname(os.path.realpath(__file__)) + f"/{file_name}"
    print(f"Plotting to {fig_path}")
    plt.savefig(fig_path)
    plt.close()


def print_grid(V, k):
    print(k)
    for i in range(size):
        print([f'{n: 5.1f}' for n in V[i * size: i * size + size]])
    ex4_2_state = CUSTOM_STATES.get(16, None)
    if ex4_2_state:
        print(['     ', f'{V[16]: 5.1f}', '     ', '     '])


def add_subplot(V, k, k_to_print):
    V = [round(v, 1) for v in V]
    v = [V[l * size: l * size + size] for l in range(size)]

    plot_index = k_to_print.index(k) if k in k_to_print else len(k_to_print)
    ax = plt.subplot((len(k_to_print) + 1),  1, plot_index + 1)
    ax.text(0.2, 0.25, f'k = {k}')
    ax.set_axis_off()
    table = plt.table(cellText=v, loc='center', cellLoc='center')
    table.scale(0.3, 3)
    if len(V) > size * size:  # HARDCODED SHIT for ex4.2
        table.add_cell(size + 1, 1, 0.3 / size, 0.6/size, text=V[16])
    ax.add_table(table)


def next_state(s, a):
    if s in CUSTOM_STATES:
        return CUSTOM_STATES[s][a], -1
    a = actions[a]
    i = s % size + a[0]
    j = s // size + a[1]
    if not (-1 < i < size) or \
       not (-1 < j < size):
        return s, -1
    next_s = i + j * size
    return next_s, -1


THETA = 1e-4  # Convergence threshold
GAMMA = 1.0   # Decay parameter


def double_arrays_v_update(V):
    new_V = [0.0] * len(V)
    for s in range(len(V)):
        if s in TERMINAL_STATES:
            continue
        ns = [next_state(s, a) for a in range(len(actions))]
        new_V[s] = sum([r + GAMMA * V[sp] for (sp, r) in ns]) * 0.25
    delta = abs(sum(V) - sum(new_V))
    return new_V, delta


def in_place_v_update(V):
    delta = 0
    for s in range(len(V)):
        v = V[s]
        if s in TERMINAL_STATES:
            continue
        ns = [next_state(s, a) for a in range(len(actions))]
        V[s] = sum([r + GAMMA * V[sp] for (sp, r) in ns]) * 0.25
        delta = abs(v - V[s])
    return V, delta


def compute(v_update_function, k_to_print=None):
    V = [0.0] * (max(CUSTOM_STATES.keys()) + 1 if CUSTOM_STATES else size * size)
    k = 0
    delta = 1
    while delta > THETA:
        V, delta = v_update_function(V)
        if k in k_to_print:
            add_subplot(V, k, k_to_print)
            print_grid(V, k)
        k += 1
    add_subplot(V, k, k_to_print)
    print_grid(V, k)


class MultipleTablePlot():
    def __init__(self, filename, title, fig_size=(8, 16)):
        self.filename = filename
        f = plt.figure(figsize=fig_size)
        plt.subplots_adjust(wspace=2.8, hspace=4)
        f.suptitle(title)

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        fig_path = os.path.dirname(os.path.realpath(__file__)) + f"/{self.filename}"
        print(f"Plotting to {fig_path}")
        plt.savefig(fig_path)
        plt.close()


if __name__ == '__main__':
    k_to_print = [0, 1, 2, 3, 10]

    with MultipleTablePlot('figure_4_1.png', '2 arrays'):
        compute(double_arrays_v_update, k_to_print=k_to_print)

    with MultipleTablePlot('figure_4_1_in_place.png', 'in place'):
        compute(in_place_v_update, k_to_print=k_to_print)

    # It is possible to override the default comportment of
    # any state inside the [0, size * size) grid (16 is hardcoded) 17 won't work
    # dict of the form {
    #   state index: (next state for actions East, West, North, South)
    # }
    CUSTOM_STATES = {
        16: (12, 14, 13, 16)
    }
    with MultipleTablePlot('exercise_4_2.png', 'Exercise 4.2', fig_size=None):
        compute(in_place_v_update, k_to_print=[])

    CUSTOM_STATES = {
        13: (12, 14, 9, 16),
        16: (12, 14, 13, 16),
    }
    with MultipleTablePlot('exercise_4_2_bis.png', 'Exercise 4.2 linked', fig_size=None):
        compute(double_arrays_v_update, k_to_print=[])
