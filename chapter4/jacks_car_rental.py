#!/usr/bin/env python3

#######################################################################
# 2019 Clement AUPETIT (clemaupetit@gmail.com)                        #
# This code was made for learning purposes                            #
# It contains programming exercises and code to produce figures       #
# Reinforcement learning: an introduction 2d edition (2018)           #
# from R.S Sutton and A.G Barto                                       #
# http://incompleteideas.net/book/the-book-2nd.html                   #
#######################################################################

import os

import numpy as np
import tqdm
from scipy.stats import poisson

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt

NUM_SHOPS = 2
RENT_INCOME = 10
MOVE_LOCATION_COST = 2
PARKING_COST = 4

MAX_POISSON = 15

RENT_RATES = (3, 4)
RETURN_RATES = (3, 2)

MAX_CARS = 20
MAX_MOVE = 5

GAMMA = 0.9

# In this code:
# s represents a single state as a tuple: (0, 0) (12, 2) ...
# a represents a single action (0, 1) => one car moves from the second location to the first

# S represents a list of all possible states of the problem in tuples [(0, 0), (0, 1) ...]

# V a dict with all possible states in tuple as key and the expected value of this state as value:
# { (0, 0): -1.00, (0, 1): 12.000, ... }

# A a dict with a state as key and a list of possible and valid actions as pair:
# { (0, 0): [(0, 0)], (0, 1): [(0, 0), (0, 1), ...]

# Pa a dict with states as key and the index of the current best action for the state in A
# it represents the policy as:
# { (0, 0): 0, (0, 1): 1, ... }

# these tuples can be treated as numpy arrays for computation: s - a  <=> (0, 1) - (0, 1) = (0, 0)


# taken from the matlab implementation:
# https://waxworksmath.com/Authors/N_Z/Sutton/RLAI_1st_Edition/Code/Chapter_4/cmpt_P_and_R.m
# https://waxworksmath.com/Authors/N_Z/Sutton/RLAI_1st_Edition/WWW/chapter_4.html
def proba_from_one_state_to_the_other(returns_lambda, rent_lambda):
    pn = np.zeros((MAX_CARS + 1, MAX_CARS + 1))
    for n_ret, p_ret in enumerate(poisson.pmf(range(MAX_POISSON), returns_lambda)):
        for n_rent, p_rent in enumerate(poisson.pmf(range(MAX_POISSON), rent_lambda)):
            for n in range(MAX_CARS + 1):
                n_next = min(max(n - n_rent, 0) + n_ret, MAX_CARS)
                pn[n][n_next] += p_ret * p_rent
    return pn

# Precompute all possible probabilities to go from one state to another for reuse in bellman equation
probas_to_next_state = (
    proba_from_one_state_to_the_other(RETURN_RATES[0], RENT_RATES[0]),
    proba_from_one_state_to_the_other(RETURN_RATES[1], RENT_RATES[1])
)


def car_move_cost_fig_4_2(a, s1):
    return sum(a) * MOVE_LOCATION_COST


def car_move_cost_ex_4_7(a, s1):
    car_move_cost = (max(a[0] - 1, 0) * MOVE_LOCATION_COST + a[1] * MOVE_LOCATION_COST)
    car_parking_cost = sum([int(n - 10 > 0) * PARKING_COST for n in s1])
    return car_move_cost + car_parking_cost


car_move_cost_function = car_move_cost_fig_4_2


def bellman_equation(S, V, s, a):
    # move cars from one location to another according to the action (assumed valid as generated in main)
    s1 = (s - a) + a[::-1]
    # reduce the cost from the cars change of location
    v = - car_move_cost_function(a, s1)
    # compute the possible expected reward for the current state (after the cars have been moved)
    reward = r(s1)
    for next_s in S:
        proba = 1.0
        for i in range(NUM_SHOPS):
            proba *= probas_to_next_state[i][s1[i]][next_s[i]]
        v += proba * (reward + GAMMA * V[next_s])
    return v


def r(s):
    reward = 0
    # num of rented cars + their income according to a poisson distribution of rental requests
    # this for each location
    for shop_id in range(NUM_SHOPS):
        possible_rents = poisson.pmf(range(MAX_POISSON), RENT_RATES[shop_id])
        for i, p_rent in enumerate(possible_rents):
            reward += min(s[shop_id], i) * RENT_INCOME * p_rent
    return reward


def policy_evaluation(S, A, V, Pa):
    delta = 1
    with tqdm.tqdm() as progress_bar:
        while delta > 1e-1:
            for s in S:
                v = V[s]
                v1 = bellman_equation(S, V, s, A[s][Pa[s]])
                delta = abs(v - v1)
                V[s] = v1
            progress_bar.set_description(f"delta: {delta: 10.5f}")
            progress_bar.update(1)
    return V


def policy_improvement(S, V, A, Pa):
    for s in tqdm.tqdm(S):
        Pa[s] = np.argmax([bellman_equation(S, V, s, a) for a in A[s]])
    return Pa


def valid_action(s, a):
    car_removed = (s - a)
    can_remove_car = car_removed > -1
    can_add_car = car_removed + a[::-1] < 21
    return np.logical_and(can_remove_car, can_add_car).all()


def plot_policy_and_values(A, Pa, V, k, name='test'):
    matA = np.zeros((MAX_CARS + 1, MAX_CARS + 1))
    matV = np.zeros((MAX_CARS + 1, MAX_CARS + 1))
    for i in range(MAX_CARS + 1):
        for j in range(MAX_CARS + 1):
            a = A[(i, j)][Pa[(i, j)]]
            matA[i][j] = a[0] - a[1]
            matV[i][j] = V[i, j]
    plt.subplot(5, 2, (k * 2) + 1)
    plt.matshow(matA, cmap='Spectral_r', origin='lower', fignum=False)
    plt.text(-15, 10, f'k = {k}')
    plt.colorbar()
    plt.subplot(5, 2, (k * 2 + 1) + 1)
    plt.matshow(matV, cmap='Spectral_r', origin='lower', fignum=False)
    plt.colorbar()
    fig_path = os.path.dirname(os.path.realpath(__file__)) + f"/{name}.png"
    print(f"Plotting to {fig_path}")
    plt.savefig(fig_path)


def policy_iteration(fig_name):
    plt.figure(figsize=(21 / 2.54, 29.7 / 2.54))

    # states = num car at each location at the end of the day (21 * 21 tuple) (0 is possible)
    S = [(i, j) for i in range(MAX_CARS + 1) for j in range(MAX_CARS + 1)]

    # actions = num of car to be moved to each other location: 6 * 6 (0 is possible)
    actions = np.asarray([(i, j) for i in range(MAX_MOVE + 1) for j in range(MAX_MOVE + 1)])

    # generate all valid actions for each s and the probability to select it
    A = {}
    Pa = {}
    for s in np.asarray(S):
        t = tuple(s)
        A[t] = [a for a in actions if valid_action(s, a)]
        Pa[t] = 0  # index of (0, 0)  action

    # one Value per state
    V = {}
    for s in S:
        V[s] = 0.0

    policy_stable = False

    k = 0
    plot_policy_and_values(A, Pa, V, k, fig_name)
    while not policy_stable:
        print('Evaluating policy')
        V = policy_evaluation(S, A, V, Pa)

        print('Improving policy')
        Pa1 = policy_improvement(S, V, A, dict(Pa))

        policy_diff = [Pa[a] == Pa1[b] for (a, b) in zip(Pa, Pa1)]
        print('policy similarity: ', policy_diff.count(True) / len(policy_diff))
        policy_stable = all(policy_diff)
        Pa = Pa1

        k += 1
        if not policy_stable:
            plot_policy_and_values(A, Pa, V, k, fig_name)


if __name__ == '__main__':
    car_move_cost_function = car_move_cost_fig_4_2
    policy_iteration('figure_4_2')

    car_move_cost_function = car_move_cost_ex_4_7
    policy_iteration('exercise_4_7')

